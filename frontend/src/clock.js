/**
 * Copyright (C) 2021 Johnny Accot <johnny.accot@iut-rodez.fr>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

import { EventEmitter } from 'events';

export default class Clock extends EventEmitter
{
    #frequency;
    #interval = null;

    constructor(frequency=1) {
        super();
        this.#frequency = frequency;  /* Hertz */
    }

    start() {
        if (this.#interval)
            throw new Error('already started');
        this.emit('start');
        this.#interval = setInterval(() => this.emit('tick'), 1000/this.#frequency);
    }

    stop() {
        if (!this.#interval)
            throw new Error('not started');
        this.emit('stop');
        clearInterval(this.#interval);
        this.#interval = null;
    }

    get frequency() {
        return this.#frequency;
    }

    set frequency(frequency) {
        /** @todo to be implemented */
        throw new Error('not implemented');
    }
}
