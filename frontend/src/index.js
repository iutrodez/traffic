/**
 * Copyright (C) 2021 Johnny Accot <johnny.accot@iut-rodez.fr>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * URLs for online-request demo:
 * - https://nominatim.openstreetmap.org/search?q=Rue+Louis+Oustry%2C+Rodez%2C+France&limit=1&format=json
 * - https://nominatim.openstreetmap.org/search?q=50+avenue+de+Bordeaux%2C+Rodez%2C+France&limit=1&format=json
 * - https://metric-osrm-backend.lab.sspcloud.fr/route/v1/driving/2.5763105,44.3499813;2.576429191630135,44.36004035?overview=full&geometries=polyline&steps=false
*/

import Car from './car';
import CarMap from './map';
import CarCanvas from './canvas';
import Clock from './clock';
import cssColors from 'css-color-names';
import polyline  from '@mapbox/polyline';
import { along, length } from '@turf/turf';
import { queryLocations, queryRoutes } from './geo';

const cssColorsNames = Object.keys(cssColors);
console.log(cssColorsNames);

const LOC_RODEZ = [ 2.576, 44.351 ];
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

/* make some symbols global so that we can use them in the browser's console */
window.CarMap = CarMap;
window.CarCanvas = CarCanvas;
window.queryLocations = queryLocations;
window.queryRoutes = queryRoutes;
window.LOC_RODEZ = LOC_RODEZ;
window.sleep = sleep;

async function main() {

    const clock = new Clock();
    clock.on('tick', () => console.log('tick'));
    clock.start();

    /* list of addresses the red car will visit */
    const addresses = [
        'Rue Louis Oustry, 12000, Rodez, France',
        '50 Avenue de Bordeaux, 12000, Rodez, France',
        'Avenue Victor Hugo, 12000, Rodez, France'
    ];

    /* resolve all addresses and build a coordinate array */
    const coordinates = [];

    for (let address of addresses) {

        const locations = await queryLocations(address);

        if (locations.length === 0)
            throw new Error(`could not resolve location ${address}`);

        coordinates.push([ locations[0].lon, locations[0].lat ]);
    }

    /* query the possible routes, select the first one and decode its geometry */
    const result = await queryRoutes(coordinates);

    if (result.routes.length === 0)
        throw new Error(`could not resolve route ${coordinates}`);

    const route = result.routes[0];
    route.geometry = polyline.toGeoJSON(route.geometry);

    /* create two cars, one red, one green */
    const red = new Car('red');
    red.moveTo(LOC_RODEZ[0], LOC_RODEZ[1]);

    const green = new Car('green');
    green.moveTo(LOC_RODEZ[0], LOC_RODEZ[1]);

    const map = new CarMap(LOC_RODEZ);
    await map.init();

    /* then move the red car for 10 seconds along its trajectory */
    const COUNT = 100;
    const TIMEOUT = 100; /* milliseconds */
    const routeLength = length(route.geometry);

    for (let i = 0; i < COUNT; ++i) {
        const point = along(route.geometry, routeLength*i/COUNT);
        const coords = point.geometry.coordinates;
        red.moveTo(coords[0], coords[1]);
        map.setCars(red, green);
        await sleep(TIMEOUT);
    }
}

main().catch(console.error);
